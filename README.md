# CS578-SkylineSociety

ASU CS578 course project (Summer 2021)

Benjamin Parrish, Eric Yu, Min Xu, Neelima Barji and Ryan Berg


# Data visualization

The data visualization files include  exploratory-analysis-education-and-hours.ipynb, explore_age_workclass_visualizations.ipynb, race_sex_visualizations.ipynb, correlation_matrix.ipynb, data-exploration-occupation-marital-status.ipynb and visualization(edNum&relationship).ipynb

# ML training 

There are 5 ML models which are implemented in svm.ipyb, knn.ipyb, decision-tree.ipyb,  Logistic_Regression_ML-2.ipyb and neural-network-tensorflow.ipynb.
