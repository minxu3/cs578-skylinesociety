#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


data = pd.read_csv("data/adult.data", index_col=False)
colNames = ["age", "workclass", "fnlwgt", "education", "educationNum", "maritalStatus",             "occupation", "relationship", "race", "sex", "capital-gain", "capital-loss",             "hoursPerWeek", "native-country", "amount"]
data.columns = colNames
# Note the first row of adult.test has been removed manually
testdata = pd.read_csv("data/adult.test", index_col=False)
testdata.columns = colNames
testdata['amount'] = np.where(testdata['amount'] == ' <=50K.', ' <=50K', '>50K')
# testdata


# #Create dummy Variables

# In[3]:


data_null= data.isnull()
data_null


# In[4]:


sns.heatmap(data.isnull())


# In[5]:


data.info()


# In[6]:


def dataFiltering(data):
    lenBefore = len(data)
    
    
# trim leading white spaces in dataframe
    cols = data.select_dtypes(['object']).columns
    data[cols] = data[cols].apply(lambda x: x.str.strip())
    
    data['target'] = np.where(data.amount == "<=50K", 0, 1)
    
# remove special characters from data    
    data = data[data["occupation"] != '?']
    data = data[data["workclass"] != '?']
    data = data[data["native-country"] != '?']

    lenAfter = len(data)

    if lenBefore != lenAfter:
         print("Length before filtering is ", lenBefore, ". Length after filter is ", lenAfter)
    # Remove two columns name is 'C' and 'D'
    data.drop(['fnlwgt', 'capital-gain', 'capital-loss'], axis = 1, inplace = True)
    
    return data


# In[8]:


# filtering data set
data = dataFiltering(data)
data[data["target"] == 0]


# In[9]:


# filtering testdata set
testdata = dataFiltering(testdata)
testdata[testdata["target"] == 0]


# In[10]:


def textToNumMap(df):
    #gender
    df['sex'] = np.where(df['sex'] == 'female', 0, 1)
    #race
    
    df['race'] = df['race'].map({'Black': 0, 'Asian-Pac-Islander': 1, 'Other': 2, 'White': 3, 'Amer-Indian-Eskimo': 4}).astype(int)
    #marital
    df['maritalStatus'] = df['maritalStatus'].map({'Married-spouse-absent': 0, 'Widowed': 1, 'Married-civ-spouse': 2, 'Separated': 3, 'Divorced': 4,'Never-married': 5, 
                                       'Married-AF-spouse': 6}).astype(int)
    #workclass
    df['workclass'] = df['workclass'].map({'Self-emp-inc': 0, 'State-gov': 1,'Federal-gov': 2, 'Without-pay': 3, 'Local-gov': 4,'Private': 5, 'Self-emp-not-inc': 6, 'Never-worked': 7}).astype(int)
    #education
    df['education'] = df['education'].map({'Some-college': 0, 'Preschool': 1, '5th-6th': 2, 'HS-grad': 3, 'Masters': 4, '12th': 5, '7th-8th': 6,  'Prof-school': 7,'1st-4th': 8, 'Assoc-acdm': 9, 'Doctorate': 10, '11th': 11,'Bachelors': 12, '10th': 13,'Assoc-voc': 14,'9th': 15}).astype(int)
    #occupation
    df['occupation'] = df['occupation'].map({ 'Farming-fishing': 1, 'Tech-support': 2, 'Adm-clerical': 3, 'Handlers-cleaners': 4, 'Prof-specialty': 5,'Machine-op-inspct': 6, 'Exec-managerial': 7,'Priv-house-serv': 8,'Craft-repair': 9,'Sales': 10, 'Transport-moving': 11, 'Armed-Forces': 12, 'Other-service': 13,'Protective-serv':14}).astype(int)
    #relationship
    df['relationship'] = df['relationship'].map({'Not-in-family': 0, 'Wife': 1, 'Other-relative': 2, 'Unmarried': 3,'Husband': 4,'Own-child': 5}).astype(int)
     # hours per week (>40)=0 (=40)=1 (>40)=2
    df['hours'] = df['hoursPerWeek'].apply(lambda x: 0 if x < 40 else (1 if x == 40 else 2))
    #native country
    countryStr = """United-States,Cambodia,England,Puerto-Rico,Canada,Germany,Outlying-US(Guam-USVI-etc),India,Japan,Greece,South,China,Cuba,Iran,Honduras,Philippines,Italy,Poland,Jamaica,Vietnam,Mexico,Portugal,Ireland,France,Dominican-Republic,Laos,Ecuador,Taiwan,Haiti,Columbia,Hungary,Guatemala,Nicaragua,Scotland,Thailand,Yugoslavia,El-Salvador,Trinadad&Tobago,Peru,Hong,Holand-Netherlands"""
  
    cList = countryStr.split(',')
#     print(cList)
    cMap = {}
    i = 0
    for a in cList:
        cMap[a] = i
        i += 1
#     print(cMap)
    #df['nativeCountry'] = df['nativeCountry'].fillna(0)
    df['native-country'] = df['native-country'].map(cMap).astype(int)
    
    return df


# In[11]:


dataF = pd.merge(data, testdata, how='outer')
dataF = textToNumMap(dataF)
dataF


# In[27]:


from sklearn.model_selection import train_test_split
# Training and Predicting
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn import metrics
def Lmodel(data, target):
    logmodel = LogisticRegression( max_iter = 1000)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, 
                                                        random_state=42)
    logmodel.fit(X_train,y_train)
#     print(X_train.shape)
#     print(X_test.shape)
#     print(y_train.shape)
#     print(y_test.shape)
    y_pred = logmodel.predict(X_test)
    Accuracy = metrics.accuracy_score(y_test, y_pred)
#     print(Accuracy)
#     print('Accuracy: ',metrics.accuracy_score(y_test, y_pred))
    # classification Report
#     print(classification_report(y_test,y_pred))
    return Accuracy


# In[28]:


X = dataF[["age", "workclass", "education", "educationNum", "maritalStatus", "occupation", "relationship", 
           "race", "sex", "hoursPerWeek", "native-country"]]
y = dataF['target']
accuracy = Lmodel(X, y)
accuracy


# In[50]:


attributes_list = ["age", "workclass", "education", "educationNum", "maritalStatus", "occupation", "relationship", 
           "race", "sex", "hoursPerWeek", "native-country"]
list = []
accuracy = []
j = 0
for i in range(len(attributes_list)):
    j+=1
    if i < len(attributes_list):
        list.append(attributes_list[i])
        
        x = dataF[list]
        y = dataF['target']
        accuracy = Lmodel(x,y)
        print("for attributes ", list," accuracy is ", accuracy)
        list.clear() 
        
        


# In[49]:


attributes_list = ["age", "workclass", "education", "educationNum", "maritalStatus", "occupation", "relationship", 
           "race", "sex", "hoursPerWeek", "native-country"]
list = []
accuracy = []
j = 0
for i in range(len(attributes_list)):
    j+=1
    if i < len(attributes_list)-1:
        list.append(attributes_list[i])
        list.append(attributes_list[i+1])
        x = dataF[list]
        y = dataF['target']
        accuracy = Lmodel(x,y)
        print("for attributes ", list," accuracy is ", accuracy)
        list.clear() 
        
        


# In[48]:


attributes_list = ["age", "workclass", "education", "educationNum", "maritalStatus", "occupation", "relationship", 
           "race", "sex", "hoursPerWeek", "native-country"]
list = []
accuracy = []
j = 0
for i in range(len(attributes_list)):
    j+=1
    if i < len(attributes_list)-2:
        list.append(attributes_list[i])
        list.append(attributes_list[i+1])
        list.append(attributes_list[i+2])
        x = dataF[list]
        y = dataF['target']
        accuracy = Lmodel(x,y)
        print("for attributes ", list," accuracy is ", accuracy)
        list.clear() 


# In[47]:


attributes_list = ["age", "workclass", "education", "educationNum", "maritalStatus", "occupation", "relationship", 
           "race", "sex", "hoursPerWeek", "native-country"]
list = []
accuracy = []

for i in range(len(attributes_list)):
    
    if i < len(attributes_list)-3:
        list.append(attributes_list[i])
        list.append(attributes_list[i+1])
        list.append(attributes_list[i+2])
        list.append(attributes_list[i+3])
        x = dataF[list]
        y = dataF['target']
        accuracy = Lmodel(x,y)
        print("for attributes ", list," accuracy is ", accuracy)
        list.clear() 
        


# In[ ]:




